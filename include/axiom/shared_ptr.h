/******************************************************************************/
/* MIT License                                                                */
/*                                                                            */
/* Copyright (c) 2019 Sergey A Erkin                                          */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom          */
/* the Software is furnished to do so, subject to the following conditions:   */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included    */
/* in all copies or substantial portions of the Software.                     */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#pragma once

#include <cstddef>
#include <cassert>

namespace axiom {

/**
 * Shared pointer to an [immutable] object across multiple instances with reference count
 */
template<class T>
class shared_ptr {
  private:
    struct Reference {
      T* value;
      mutable int count;
    };
    Reference* ref;

  public:
    /**
     * ctor
     * @param value - value for this pointer (never null)
     */
    explicit shared_ptr(T* value) {
      assert(value);
      ref = new Reference();
      ref->value = value;
      ref->count = 1;
    }

    /**
     * copy ctor
     * @param other
     */
    shared_ptr(const shared_ptr& other) {
      ref = other.ref;
      ref->count++;
    }

    /**
     * copy operator
     * @param other
     */
    shared_ptr& operator=(const shared_ptr& other) {
      if (this != &other) {
        decrement();
        ref = other.ref;
        ref->count++;
      }
      return *this;
    }

    /**
     * @return value held by this pointer
     */
    T& value() {
      return *ref->value;
    }

    /**
     * @return value held by this pointer
     */
    const T& value() const {
      return *ref->value;
    }

    operator T*() {
      return ref->value;
    }

    T& operator*() {
      return *ref->value;
    }

    T* operator->() {
      return ref->value;
    }

    operator const T*() const {
      return ref->value;
    }

    const T& operator*() const {
      return *ref->value;
    }

    const T* operator->() const {
      return ref->value;
    }

    /**
     * @return count of instances pointed to the same value
     */
    int count() const {
      return ref->count;
    }

    ~shared_ptr() {
      decrement();
    }

  private:
    void decrement() {
      ref->count--;
      if (ref->count == 0) {
        delete ref->value;
        delete ref;
        ref = NULL;
      }
    }
};

}