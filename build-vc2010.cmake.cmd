@mkdir build-vc2010
@pushd build-vc2010
cmake build-vc2010 -G "Visual Studio 10 2010" ..\
cmake --build . --config Release --target ALL_BUILD
ctest -C Release
@popd