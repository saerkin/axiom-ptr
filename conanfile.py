# noinspection PyUnresolvedReferences
from conans import ConanFile

class AxiomShareConan(ConanFile):
  name = "axiom-ptr"
  version = "0.1.0"
  license = "MIT"
  author = "Sergey A. Erkin (sergey.a.erkin@gmail.com)"
  url = "https://saerkin@bitbucket.org/saerkin/axiom-ptr.git"
  settings = "os", "compiler", "build_type", "arch"
  exports_sources = "include/*"
  no_copy_source = True
  generators = "cmake"
  build_requires = \
    "Catch/1.12.2@bincrafters/stable"

  def package(self):
    self.copy("*.h")

  def package_id(self):
    self.info.header_only()
