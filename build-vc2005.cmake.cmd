@mkdir build-vc2005
@pushd build-vc2005
cmake build-vc2005 -G "Visual Studio 8 2005" ..\
cmake --build . --config Release --target ALL_BUILD
ctest -C Release
@popd