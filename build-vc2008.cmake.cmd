@mkdir build-vc2008
@pushd build-vc2008
cmake build-vc2008 -G "Visual Studio 9 2008" ..\
cmake --build . --config Release --target ALL_BUILD
ctest -C Release
@popd