#pragma clang diagnostic push
#pragma ide diagnostic ignored "performance-unnecessary-copy-initialization"
#include <catch.hpp>
#include "axiom/shared_ptr.h"

using namespace axiom;

class Object {
  public:
    bool* deleted; // NOLINT(misc-non-private-member-variables-in-classes)

    Object() { deleted = NULL; }

    explicit Object(bool* deleted) {
      this->deleted = deleted;
      *this->deleted = false;
    }

    ~Object() {
      if (deleted != NULL) {
        *deleted = true;
      }
    }
};

TEST_CASE("Every share instance has same value", "[axiom::shared_ptr]") {
  Object* o1 = new Object();
  shared_ptr<Object> p11(o1);
  shared_ptr<Object> p12 = p11;
  shared_ptr<Object> p13 = p12;
  REQUIRE(&p11.value() == o1);
  REQUIRE(&p12.value() == o1);
  REQUIRE(&p13.value() == o1);

  Object* o2 = new Object();
  shared_ptr<Object> p21(o2);
  shared_ptr<Object> p22 = p21;
  REQUIRE(&p21.value() == o2);
  REQUIRE(&p22.value() == o2);

  REQUIRE(&p13.value() != &p21.value());
}

TEST_CASE("Every share instance has same ref.count", "[axiom::shared_ptr]") {
  shared_ptr<Object> p1(new Object());
  shared_ptr<Object> p2 = p1;
  shared_ptr<Object> p3 = p2;
  REQUIRE(p1.count() == 3);
  REQUIRE(p2.count() == 3);
  REQUIRE(p3.count() == 3);
}

TEST_CASE("Reference count increments on new share", "[axiom::shared_ptr]") {
  shared_ptr<Object> p1(new Object());
  REQUIRE(p1.count() == 1);

  shared_ptr<Object> p2 = p1;
  REQUIRE(p1.count() == 2);

  shared_ptr<Object> p3 = p2;
  REQUIRE(p1.count() == 3);
}

TEST_CASE("Reference count decrements on share delete", "[axiom::shared_ptr]") {
  shared_ptr<Object> p1(new Object());
  {
    shared_ptr<Object> p2 = p1;
    {
      shared_ptr<Object> p3 = p2;
      REQUIRE(p1.count() == 3);
    }
    REQUIRE(p1.count() == 2);
  }
  REQUIRE(p1.count() == 1);
}

TEST_CASE("Destructor don't run when ref.count > 0", "[axiom::shared_ptr]") {
  bool objIsDeleted = false;
  shared_ptr<Object> p1(new Object(&objIsDeleted));
  {
    shared_ptr<Object> p2 = p1;
  }
  REQUIRE(!objIsDeleted);
}

TEST_CASE("Destructor runs when ref.count == 0", "[axiom::shared_ptr]") {
  bool objIsDeleted = false;
  {
    shared_ptr<Object> p1(new Object(&objIsDeleted));
    shared_ptr<Object> p2 = p1;
  }
  REQUIRE(objIsDeleted);
}

#pragma clang diagnostic pop